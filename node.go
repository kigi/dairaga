// +build js,wasm

package dairaga

import "syscall/js"

// Bitmask of Document Position
const (
	DocumentPositionDisConnected = 1 << iota
	DocumentPositionPreceding
	DocumentPositionFollowing
	DocumentPositionContains
	DocumentPositionContainedBy
	DocumentPositionImplementationSpecific
)

// Node type constants
const (
	ElementNode = 1 + iota
	AttributeNode
	TextNode
	CDataSectionNode
	EntityReferenceNode
	EntityNode
	ProcessingInstructionNode
	CommentNode
	DocumentNode
	DocumentTypeNode
	DocumentFragmentNode
	NotationNode
)

// Node javascript Node (https://developer.mozilla.org/en-US/docs/Web/API/Node)
type Node interface {
	EventTarget

	// properties

	BaseURI() string
	ChildNodes() NodeList
	FirstChild() Node
	InnerText() string
	SetInnerText(string)
	IsConnected() bool
	LastChild() Node
	NodeName() string
	NodeType() int
	NodeValue() string
	OwnerDocument() Document
	ParentElement() Element
	ParentNode() Node
	PreviousSibling() Node
	TextContent() string
	SetTextContent(string)

	// TODO: methods
	/*
		lookupNamespaceURI()
		lookupPrefix()
	*/
	AppendChild(Node) Node
	CloneNode(bool) Node
	CompareDocumentPosition(Node) int
	Contains(Node) bool
	GetRootNode(map[string]bool) Node
	HasChildNodes() bool
	InsertBefore(Node, Node)
	IsDefaultNamespace(string) bool
	IsEqualNode(Node) bool
	IsSameNode(Node) bool
	Normalize()
	RemoveChild(Node) Node
	ReplaceChild(Node, Node) Node
}

type _node struct {
	EventTarget
}

// NodeOf convert js.Value to Node
func NodeOf(v JSValue) Node {
	return _node{EventTargetOf(v)}
}

// Properties

func (n _node) BaseURI() string {
	return n.Get("baseURI").String()
}

func (n _node) ChildNodes() NodeList {
	v := n.Get("childNodes")

	if v == _null || v == _undefined {
		return nil
	}

	return NodeListOf(v)
}

func (n _node) FirstChild() Node {
	return NodeOf(n.Get("firstChild"))
}

func (n _node) InnerText() string {
	return n.Get("innerText").String()
}

func (n _node) SetInnerText(text string) {
	n.Set("innerText", text)
}

func (n _node) IsConnected() bool {
	return n.Get("isConnected").Bool()
}

func (n _node) LastChild() Node {
	return NodeOf(n.Get("lastChild"))
}

func (n _node) NextSibling() Node {
	return NodeOf(n.Get("nextSibling"))
}

func (n _node) NodeName() string {
	return NodeOf(n.Get("nodeName")).String()
}

func (n _node) NodeType() int {
	return n.Get("nodeType").Int()
}

func (n _node) NodeValue() string {
	return n.Get("nodeValue").String()
}

func (n _node) OwnerDocument() Document {
	v := n.Get("ownerDocument")
	return DocumentOf(v)
}

func (n _node) ParentElement() Element {
	return ElementOf(n.Get("parentElement"))
}

func (n _node) ParentNode() Node {
	return NodeOf(n.Get("parentNode"))
}

func (n _node) PreviousSibling() Node {
	return NodeOf(n.Get("previousSibling"))
}

func (n _node) TextContent() string {
	return n.Get("textContent").String()
}

func (n _node) SetTextContent(con string) {
	n.Set("textContent", con)
}

// Methods

func (n _node) AppendChild(x Node) Node {
	n.Call("appendChild", x.ToValue())
	return x
}

func (n _node) CloneNode(deep bool) Node {
	return NodeOf(EventTargetOf(n.Call("cloneNode", deep)))
}

func (n _node) CompareDocumentPosition(other Node) int {
	return n.Call("compareDocumentPosition", other.ToValue()).Int()
}

func (n _node) Contains(other Node) bool {
	return n.Call("contains", other.ToValue()).Bool()
}

func (n _node) GetRootNode(options map[string]bool) Node {
	return NodeOf(n.Call("getRootNode", options))
}

func (n _node) HasChildNodes() bool {
	return n.Call("hasChildNodes").Bool()
}

func (n _node) InsertBefore(newNode, ref Node) {
	if ref == nil {
		n.Call("insertBefore", newNode.ToValue(), js.Null())
		return
	}

	n.Call("insertBefore", newNode.ToValue(), ref.ToValue())
}

func (n _node) IsDefaultNamespace(uri string) bool {
	return n.Call("isDefaultNamespace", uri).Bool()
}

func (n _node) IsEqualNode(other Node) bool {
	return n.Call("isEqualNode", other.ToValue()).Bool()
}

func (n _node) IsSameNode(other Node) bool {
	return n.Call("isSameNode", other.ToValue()).Bool()
}

func (n _node) Normalize() {
	n.Call("normalize")
}

func (n _node) RemoveChild(child Node) Node {
	return NodeOf(n.Call("removeChild", child.ToValue()))
}

func (n _node) ReplaceChild(newChild, oldChild Node) Node {
	if newChild == nil {
		return NodeOf(n.Call("replaceChild", js.Null(), oldChild.ToValue()))
	}

	return NodeOf(n.Call("replaceChild", newChild.ToValue(), oldChild.ToValue()))
}
