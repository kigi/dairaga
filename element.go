// +build js,wasm

package dairaga

// A string representing the position relative to the element
const (
	PosBeforeBegin = "beforebegin"
	PosAfterBegin  = "afterbegin"
	PosBeforeEnd   = "beforeend"
	PosAfterEnd    = "afterend"
)

// Element javascript Element (https://developer.mozilla.org/en-US/docs/Web/API/Element)
type Element interface {
	Node

	// TODO: properties
	/*
		onfullscreenchange
		onfullscreenerror
		ongotpointercapture
		onlostpointercapture

		shadowRoot
		slot
	*/
	Attributes() map[string]string
	ChildElementCount() uint64
	Children() []Node
	ClassList() DOMTokenList
	ClassName() string
	SetClassName(string)
	ClientHeight() int
	ClientLeft() int
	ClientTop() int
	ClientWidth() int
	FirstElementChild() Element
	ID() string
	InnerHTML() string
	SetInnerHTML(string)
	LastElementChild() Element
	LocalName() string
	Name() string
	NamespaceURI() string
	NextElementSibling() Element
	OuterHTML() string
	SetOuterHTML(string)
	Prefix() string
	PreviousElementSibling() Element
	ScrollHeight() int
	ScrollLeft() int
	ScrollTop() int
	ScrollWidth() int
	TagName() string

	// TODO: methods
	/*
		animate()
		attachShadow()

		computedStyleMap()
		hasPointerCapture()
		releasePointerCapture()
		requestFullscreen()
		requestPointerLock()

		setCapture()
		setPointerCapture()
	*/
	After(interface{})
	Append(interface{})
	Before(interface{})
	Closest(string) Element
	GetAttribute(string) string
	GetAttributeNames() []string
	GetAttributeNode(string) Node
	GetAttributeNodeNS(string, string) Node
	GetAttributeNS(string, string) string
	GetBoundingClientRect() DOMRect
	GetClientRects() []DOMRect
	GetElementsByClassName(string) []Element
	GetElementsByTagName(string) []Element
	GetElementsByTagNameNS(string, string) []Element
	HasAttribute(string) bool
	HasAttributeNS(string, string) bool
	HasAttributes() bool
	InsertAdjacentElement(string, Element) Element
	InsertAdjacentHTML(string, string)
	InsertAdjacentText(string, string)
	Matches(string) bool
	Prepend(interface{})
	QuerySelector(string) Element
	QuerySelectorAll(string) ElementList
	Remove()
	RemoveAttribute(string)
	RemoveAttributeNode(Node) Node
	RemoveAttributeNS(string, string)
	ReplaceWith(interface{})
	Scroll(int, int)
	ScrollWithOpt()
	ScrollBy(int, int)
	ScrollByWithOpt()
	ScrollTo(int, int)
	ScrollToWithOpt()
	SetAttribute(string, string)
	SetAttributeNode(Node) Node
	SetAttributeNodeNS(Node) Node
	SetAttributeNS(string, string, string)
}

type _element struct {
	Node
}

// ElementOf ...
func ElementOf(v JSValue) Element {
	return _element{
		NodeOf(v),
	}
}

// Properties

func (e _element) Attributes() map[string]string {
	if !e.HasAttributes() {
		return nil
	}

	attr := e.Get("attributes")
	ret := make(map[string]string)
	size := attr.Length()
	for i := 0; i < size; i++ {
		key := attr.Index(i).Get("name").String()
		value := attr.Index(i).Get("value").String()
		ret[key] = value
	}

	return ret
}

func (e _element) ChildElementCount() uint64 {
	return uint64(e.Get("childElementCount").Int())
}

func (e _element) Children() []Node {
	v := e.Get("children")
	if v == _null || v == _undefined {
		return nil
	}
	size := v.Length()
	if size == 0 {
		return nil
	}

	ret := make([]Node, size)
	for i := 0; i < size; i++ {
		ret[i] = NodeOf(v.Index(i))
	}

	return ret
}

func (e _element) ClassList() DOMTokenList {
	return DOMTokenListOf(e.Get("classList"))
}

func (e _element) ClassName() string {
	return e.Get("className").String()
}

func (e _element) SetClassName(name string) {
	e.Set("className", name)
}

func (e _element) ClientHeight() int {
	return e.Get("clientHeight").Int()
}

func (e _element) ClientLeft() int {
	return e.Get("clientLeft").Int()
}

func (e _element) ClientTop() int {
	return e.Get("clientTop").Int()
}

func (e _element) ClientWidth() int {
	return e.Get("clientWidth").Int()
}

func (e _element) FirstElementChild() Element {
	return ElementOf(e.Get("firstElementChild"))
}

func (e _element) ID() string {
	return e.Get("id").String()
}

func (e _element) InnerHTML() string {
	return e.Get("innerHTML").String()
}

func (e _element) SetInnerHTML(h string) {
	e.Set("innerHTML", h)
}

func (e _element) LastElementChild() Element {
	return ElementOf(e.Get("lastElementChild"))
}

func (e _element) LocalName() string {
	return e.Get("localName").String()
}

func (e _element) Name() string {
	return e.Get("name").String()
}

func (e _element) NamespaceURI() string {
	return e.Get("namespaceURI").String()
}

func (e _element) NextElementSibling() Element {
	return ElementOf(e.Get("nextElementSibling"))
}

func (e _element) OuterHTML() string {
	return e.Get("outerHTML").String()
}

func (e _element) SetOuterHTML(htmlStr string) {
	e.Set("outerHTML", htmlStr)
}

func (e _element) Prefix() string {
	return e.Get("prefix").String()
}

func (e _element) PreviousElementSibling() Element {
	return ElementOf(e.Get("previousElementSibling"))
}

func (e _element) ScrollHeight() int {
	return e.Get("scrollHeight").Int()
}

func (e _element) ScrollLeft() int {
	return e.Get("scrollLeft").Int()
}

func (e _element) ScrollTop() int {
	return e.Get("scrollTop").Int()
}

func (e _element) ScrollWidth() int {
	return e.Get("scrollWidth").Int()
}

func (e _element) TagName() string {
	return e.Get("tagName").String()
}

// Methods

func (e _element) After(n interface{}) {
	callInterfaceValue(e, "after", n)
}

func (e _element) Append(n interface{}) {
	callInterfaceValue(e, "append", n)
}

func (e _element) Before(n interface{}) {
	callInterfaceValue(e, "before", n)
}

func (e _element) Closest(q string) Element {
	return ElementOf(e.Call("closest", q))
}

func (e _element) GetAttribute(attr string) string {
	return e.Call("getAttribute", attr).String()
}

func (e _element) GetAttributeNames() []string {
	return StringSliceOf(e.Call("getAttributeNames"))
}

func (e _element) GetAttributeNode(attr string) Node {
	return NodeOf(e.Call("getAttributeNode", attr))
}

func (e _element) GetAttributeNodeNS(ns string, attr string) Node {
	return NodeOf(e.Call("getAttributeNodeNS", ns, attr))
}

func (e _element) GetAttributeNS(ns string, attr string) string {
	return e.Call("getAttributeNS", ns, attr).String()
}

func (e _element) GetBoundingClientRect() DOMRect {
	return DOMRectOf(e.Call("getBoundingClientRect"))
}

func (e _element) GetClientRects() []DOMRect {
	v := e.Call("getClientRects")
	if v == _null || v == _undefined {
		return nil
	}

	size := v.Length()
	if size <= 0 {
		return nil
	}

	var ret []DOMRect

	for i := 0; i < size; i++ {
		ret = append(ret, DOMRectOf(v.Index(i)))
	}

	return ret
}

func (e _element) GetElementsByClassName(name string) []Element {
	return collectionToElements(e.Call("getElementsByClassName", name))
}

func (e _element) GetElementsByTagName(tag string) []Element {
	return collectionToElements(e.Call("getElementsByTagName", tag))
}

func (e _element) GetElementsByTagNameNS(ns, tag string) []Element {
	return collectionToElements(e.Call("getElementsByTagNameNS", ns, tag))
}

func (e _element) HasAttribute(name string) bool {
	return e.Call("hasAttribute", name).Bool()
}

func (e _element) HasAttributeNS(ns, name string) bool {
	return e.Call("hasAttributeNS", ns, name).Bool()
}

func (e _element) HasAttributes() bool {
	return e.Call("hasAttributes").Bool()
}

func (e _element) InsertAdjacentElement(pos string, elm Element) Element {
	v := e.Call("insertAdjacentElement", pos, elm.ToValue())
	if v == _null {
		return nil
	}
	return ElementOf(v)
}

func (e _element) InsertAdjacentHTML(pos string, htmlStr string) {
	e.Call("insertAdjacentHTML", pos, htmlStr)
}

func (e _element) InsertAdjacentText(pos string, textStr string) {
	e.Call("insertAdjacentText", pos, textStr)
}

func (e _element) Matches(q string) bool {
	return e.Call("matches", q).Bool()
}

func (e _element) Prepend(n interface{}) {
	callInterfaceValue(e, "prepend", n)
}

func (e _element) QuerySelector(q string) Element {
	return ElementOf(e.Call("querySelector", q))
}

func (e _element) QuerySelectorAll(q string) ElementList {
	return ElementListOf(e.Call("querySelectorAll", q))
}

func (e _element) Remove() {
	e.Call("remove")
}

func (e _element) RemoveAttribute(attr string) {
	e.Call("removeAttribute", attr)
}

func (e _element) RemoveAttributeNode(attr Node) Node {
	return NodeOf(e.Call("removeAttributeNode", attr.ToValue()))
}

func (e _element) RemoveAttributeNS(ns string, attr string) {
	e.Call("removeAttributeNS", ns, attr)
}

func (e _element) ReplaceWith(n interface{}) {
	callInterfaceValue(e, "replaceWith", n)
}

func (e _element) Scroll(x, y int) {
	e.Call("scroll", x, y)
}

func (e _element) ScrollWithOpt() {
	// TODO: scroll with opt (https://developer.mozilla.org/en-US/docs/Web/API/ScrollToOptions)
}

func (e _element) ScrollBy(x, y int) {
	e.Call("scrollBy", x, y)
}

func (e _element) ScrollByWithOpt() {
	// TODO: scroll with opt (https://developer.mozilla.org/en-US/docs/Web/API/ScrollToOptions)
}

func (e _element) ScrollTo(x, y int) {
	e.Call("scrollTo", x, y)
}

func (e _element) ScrollToWithOpt() {
	// TODO: scroll with opt (https://developer.mozilla.org/en-US/docs/Web/API/ScrollToOptions)
}

func (e _element) SetAttribute(name, value string) {
	e.Call("setAttribute", name, value)
}

func (e _element) SetAttributeNode(n Node) Node {
	return NodeOf(e.Call("setAttributeNode", n.ToValue()))
}

func (e _element) SetAttributeNodeNS(n Node) Node {
	return NodeOf(e.Call("setAttributeNodeNS", n.ToValue()))
}

func (e _element) SetAttributeNS(ns, name, value string) {
	e.Call("setAttributeNS", ns, name, value)
}
