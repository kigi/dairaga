// +build js,wasm

package dairaga

// Event phase constants (https://developer.mozilla.org/en-US/docs/Web/API/Event/eventPhase)
const (
	EventNone = iota
	EventCapturingPhase
	EventAtTarget
	EventBubblingPhase
)

// Event javascript event (https://developer.mozilla.org/en-US/docs/Web/API/Event)
type Event interface {
	JSValue

	// properties

	Bubbles() bool
	Cancelable() bool
	CancelBubble() bool
	SetCancelBubble(bool)
	Composed() bool
	CurrentTarget() Element
	EventPhase() int
	IsTrusted() int
	ReturnValue() bool
	SetReturnValue(bool)
	SrcElement() Element
	Target() Element
	TimeStamp() int64
	Type() string

	// methods

	ComposedPath() []EventTarget
	PreventDefault()
	StopImmediatePropagation()
	StopPropagation()
}

type _event struct {
	JSValue
}

// Properties

func (e _event) Bubbles() bool {
	return e.Get("bubbles").Bool()
}

func (e _event) Cancelable() bool {
	return e.Get("cancelable").Bool()
}

func (e _event) CancelBubble() bool {
	return e.Get("cancelBubble").Bool()
}

func (e _event) SetCancelBubble(flag bool) {
	e.Set("cancelBubble", flag)
}

func (e _event) Composed() bool {
	return e.Get("composed").Bool()
}

func (e _event) CurrentTarget() Element {
	return ElementOf(e.Get("currentTarget"))
}

func (e _event) EventPhase() int {
	return e.Get("eventPhase").Int()
}

func (e _event) IsTrusted() int {
	return e.Get("isTrusted").Int()
}

func (e _event) ReturnValue() bool {
	return e.Get("returnValue").Bool()
}

func (e _event) SetReturnValue(flag bool) {
	e.Set("returnValue", flag)
}

func (e _event) SrcElement() Element {
	// same as target
	return ElementOf(e.Get("target"))
}

func (e _event) Target() Element {
	return ElementOf(e.Get("target"))
}

func (e _event) TimeStamp() int64 {
	return int64(e.Get("timeStamp").Int())
}

func (e _event) Type() string {
	return e.Get("type").String()
}

// Methods

func (e _event) ComposedPath() []EventTarget {
	v := e.Call("composedPath")
	if !IsArray(v) {
		return nil
	}

	size := v.Length()
	if size <= 0 {
		return nil
	}

	ret := make([]EventTarget, size)

	for i := 0; i < size; i++ {
		ret[i] = EventTargetOf(v.Index(i))
	}
	return ret
}

func (e _event) PreventDefault() {
	e.Call("preventDefault")
}

func (e _event) StopImmediatePropagation() {
	e.Call("stopImmediatePropagation")
}

func (e _event) StopPropagation() {
	e.Call("stopPropagation")
}

// EventOf conver js.Value to Event
func EventOf(v JSValue) Event {
	return _event{v}
}
