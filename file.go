// +build js,wasm

package dairaga

import (
	"time"
)

// File javascript File (https://developer.mozilla.org/en-US/docs/Web/API/File)
type File interface {
	Blob

	// TODO: properties
	/*
		lastModified
		lastModifiedDate
		mozFullPath
		webkitRelativePath
	*/
	Name() string
	LastModified() time.Time
}

type _file struct {
	Blob
}

func (f _file) Name() string {
	return f.Get("name").String()
}

func (f _file) LastModified() time.Time {
	m := int64(f.Get("lastModified").Int())
	return time.Unix(0, m*int64(time.Millisecond))
}

// FileOf convert js.Value to File
func FileOf(v JSValue) File {
	return _file{
		BlobOf(v),
	}
}
