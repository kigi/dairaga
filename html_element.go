// +build js,wasm

package dairaga

import (
	"syscall/js"
)

// HTMLElement.contentEditable property (https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/contentEditable)
const (
	ContentEditableTrue    = "true"
	ContentEditableFalse   = "false"
	ContentEditableInherit = "inherit"
)

// The HTMLElement.dir property (https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dir)
const (
	DirLeftToRight = "ltr"
	DirRightToLeft = "rtl"
	DirAuto        = "auto"
)

// HTMLElement javascript HTMLElement (https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)
type HTMLElement interface {
	Element

	// TODO: properties
	/*
		dataset
		onabort

		onanimationend
		onanimationiteration
		onauxclick
		onblur

		onclose
		oncontextmenu
		oncopy
		oncut
		ondblclick
		onerror
		onfocus
		ongotpointercapture
		oninput
		onkeydown
		onkeypress
		onkeyup

		onloadend
		onloadstart
		onlostpointercapture
		onmousedown
		onmousemove
		onmouseout
		onmouseover
		onmouseup
		onpaste
		onpointercancel
		onpointerdown
		onpointerenter
		onpointerleave
		onpointermove
		onpointerout
		onpointerover
		onpointerup
		onreset
		onresize
		onscroll
		onselect
		onselectionchange
		onselectstart
		onsubmit
		ontouchcancel
		ontouchstart
		ontransitioncancel
		ontransitionend
		onwheel

		style
	*/

	ContentEditable() string
	SetContentEditable(string)
	Dir() string
	SetDir(string)
	Hidden() bool
	SetHidden(bool)
	IsContentEditable() bool
	Lang() string
	SetLang(string)
	OffsetHeight() int
	OffsetLeft() int
	OffsetParent() Element
	OffsetTop() int
	OffsetWidth() int
	TabIndex() int
	SetTabIndex(int)
	Title() string
	SetTitle(string)

	// methods
	Blur()
	Click()
	Focus()

	// Events
	OnAnimationCancel(func(Event)) js.Callback
	OnChange(func(Event)) js.Callback
	OnClick(func(MouseEvent)) js.Callback
}

type _htmlElement struct {
	Element
}

func (h _htmlElement) ContentEditable() string {
	return h.Get("contentEditable").String()
}

func (h _htmlElement) SetContentEditable(value string) {
	h.Set("contentEditable", value)
}

func (h _htmlElement) Dir() string {
	return h.Get("dir").String()
}

func (h _htmlElement) SetDir(d string) {
	h.Set("dir", d)
}

func (h _htmlElement) Hidden() bool {
	return h.Get("hidden").Bool()
}

func (h _htmlElement) SetHidden(flag bool) {
	h.Set("hidden", flag)
}

func (h _htmlElement) IsContentEditable() bool {
	return h.Get("isContentEditable").Bool()
}

func (h _htmlElement) Lang() string {
	return h.Get("lang").String()
}

func (h _htmlElement) SetLang(lang string) {
	h.Set("lang", lang)
}

func (h _htmlElement) OffsetHeight() int {
	return h.Get("offsetHeight").Int()
}

func (h _htmlElement) OffsetLeft() int {
	return h.Get("offsetLeft").Int()
}

func (h _htmlElement) OffsetParent() Element {
	return ElementOf(h.Get("offsetParent"))
}

func (h _htmlElement) OffsetTop() int {
	return h.Get("offsetTop").Int()
}

func (h _htmlElement) OffsetWidth() int {
	return h.Get("offsetWidth").Int()
}

func (h _htmlElement) TabIndex() int {
	return h.Get("tabIndex").Int()
}

func (h _htmlElement) SetTabIndex(idx int) {
	h.Set("tabIndex", idx)
}

func (h _htmlElement) Title() string {
	return h.Get("title").String()
}

func (h _htmlElement) SetTitle(t string) {
	h.Set("title", t)
}

// Methods

func (h _htmlElement) Blur() {
	h.Call("blur")
}

func (h _htmlElement) Click() {
	h.Call("click")
}

func (h _htmlElement) Focus() {
	h.Call("focus")
}

// Events

//func (h _htmlElement) OnAnimationCancel(f func(Event)) js.Callback {
//	return addEventCB(h, "animationcancel", f)
//}

func (h _htmlElement) OnChange(f func(Event)) js.Callback {
	return addEventCB(h, "change", f)
}

func (h _htmlElement) OnClick(f func(MouseEvent)) js.Callback {
	return addMouseEventCB(h, "click", f)
}

func (h _htmlElement) OnLoad(f func(Event)) js.Callback {
	return addEventCB(h, "load", f)
}

// HTMLElementOf convert js.Value to HTMLElement
func HTMLElementOf(v JSValue) HTMLElement {
	return _htmlElement{
		ElementOf(v),
	}
}
