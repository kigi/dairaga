// +build js,wasm

package dairaga

import "syscall/js"

// JSValue wrap js.Value
type JSValue interface {
	ToValue() js.Value
	ValueType() js.Type

	// Warp js.Value functions

	Bool() bool
	Call(string, ...interface{}) JSValue
	Float() float64
	Get(string) JSValue
	Index(int) JSValue
	InstanceOf(JSValue) bool
	Int() int
	Invoke(...interface{}) JSValue
	Length() int
	New(...interface{}) JSValue
	Set(string, interface{})
	SetIndex(int, interface{})
	String() string
}

type _jsValue js.Value

// ValueOf convert js.Value to JSValue
func ValueOf(v js.Value) JSValue {
	return _jsValue(v)
}

func (v _jsValue) ToValue() js.Value {
	return js.Value(v)
}

func (v _jsValue) ValueType() js.Type {
	return js.Value(v).Type()
}

func (v _jsValue) Bool() bool {
	return js.Value(v).Bool()
}

func (v _jsValue) Call(m string, p ...interface{}) JSValue {
	return _jsValue(js.Value(v).Call(m, p...))
}

func (v _jsValue) Float() float64 {
	return js.Value(v).Float()
}

func (v _jsValue) Get(key string) JSValue {
	return _jsValue(js.Value(v).Get(key))
}

func (v _jsValue) Index(i int) JSValue {
	return _jsValue(js.Value(v).Index(i))
}

func (v _jsValue) InstanceOf(other JSValue) bool {
	return js.Value(v).InstanceOf(other.ToValue())
}

func (v _jsValue) Int() int {
	return js.Value(v).Int()
}

func (v _jsValue) Invoke(p ...interface{}) JSValue {
	return _jsValue(js.Value(v).Invoke(p...))
}

func (v _jsValue) Length() int {
	return js.Value(v).Length()
}

func (v _jsValue) New(p ...interface{}) JSValue {
	return _jsValue(js.Value(v).New(p...))
}

func (v _jsValue) Set(key string, value interface{}) {
	js.Value(v).Set(key, value)
}

func (v _jsValue) SetIndex(i int, value interface{}) {
	js.Value(v).SetIndex(i, value)
}

func (v _jsValue) String() string {
	return js.Value(v).String()
}
