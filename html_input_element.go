// +build js,wasm

package dairaga

import (
	"syscall/js"
)

// HTMLInputElement javascript HTMLInputElement (https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement)
type HTMLInputElement interface {
	HTMLElement

	// TODO: properties
	/*
		validity
		validationMessage
		willValidate

		// radio,checkbox
		indeterminate

		// image
		alt
		height
		src
		width
	*/

	// common
	Labels() []Node

	Multiple() bool

	//Name() string
	SetName(string) string

	Type() string
	SetType(typ string) string

	Value() string
	SetValue(string) string

	// for not hidden input
	Disabled() bool
	SetDisabled(bool) bool

	AutoFocus() bool
	SetAutoFocus(bool) bool

	Required() bool
	SetRequired(bool) bool

	// for radio or checkbox
	Checked() bool
	SetChecked(bool) bool

	DefaultChecked() bool
	SetDefaultChecked(bool) bool

	// file
	Accept() string
	SetAccept(string) string
	Files() []File

	// TODO: methods
	/*
		select()
		setSelectionRange()
	*/

	// TODO: events
	/*
		abort
		DOMContentLoaded
		afterprint
		afterscriptexecute
		beforeprint
		beforescriptexecute
		beforeunload
		blur
		cancel
		click
		close
		connect
		contextmenu
		error
		focus
		hashchange
		input
		invalid
		languagechange
		load
		loadend
		loadstart
		message
		offline
		online
		open
		pagehide
		pageshow
		popstate
		progress
		readystatechange
		reset
		select
		show
		sort
		storage
		submit
		toggle
		unload
		loadeddata
		loadedmetadata
		canplay
		playing
		play
		canplaythrough
		seeked
		seeking
		stalled
		suspend
		timeupdate
		volumechange
		waiting
		durationchange
		emptied
		unhandledrejection
		rejectionhandled
	*/
}

type _htmlInputElement struct {
	HTMLElement
}

// Properties

func (h _htmlInputElement) Labels() []Node {
	v := h.Get("labels")
	if v.ToValue() == js.Undefined() || v.ToValue() == js.Null() {
		return nil
	}

	len := v.Get("length").Int()
	ret := make([]Node, len)

	for i := 0; i < len; i++ {
		ret[i] = NodeOf(v.Index(i))
	}
	return ret
}

func (h _htmlInputElement) Multiple() bool {
	v := h.Get("multiple")
	if v.ToValue() == js.Undefined() {
		return false
	}
	return v.Bool()
}

func (h _htmlInputElement) Name() string {
	return h.Get("name").String()
}

func (h _htmlInputElement) SetName(name string) string {
	h.Set("name", name)
	return h.Name()
}

func (h _htmlInputElement) Type() string {
	return h.Get("type").String()
}

func (h _htmlInputElement) SetType(typ string) string {
	h.Set("type", typ)
	return h.Type()
}

func (h _htmlInputElement) Disabled() bool {
	return h.Get("disabled").Bool()
}

func (h _htmlInputElement) SetDisabled(flag bool) bool {
	h.Set("disabled", flag)
	return h.Disabled()
}

func (h _htmlInputElement) AutoFocus() bool {
	return h.Get("autofocus").Bool()
}

func (h _htmlInputElement) SetAutoFocus(flag bool) bool {
	h.Set("autofocus", flag)
	return h.AutoFocus()
}

func (h _htmlInputElement) Required() bool {
	return h.Get("required").Bool()
}

func (h _htmlInputElement) SetRequired(flag bool) bool {
	h.Set("required", flag)
	return h.Required()
}

func (h _htmlInputElement) Value() string {
	return h.Get("value").String()
}

func (h _htmlInputElement) SetValue(newValue string) string {
	h.Set("value", newValue)
	return h.Value()
}

func (h _htmlInputElement) Checked() bool {
	return h.Get("checked").Bool()
}

func (h _htmlInputElement) SetChecked(flag bool) bool {
	h.Set("checked", flag)
	return h.Checked()
}

func (h _htmlInputElement) DefaultChecked() bool {
	return h.Get("defaultChecked").Bool()
}

func (h _htmlInputElement) SetDefaultChecked(flag bool) bool {
	h.Set("defaultChecked", flag)
	return h.DefaultChecked()
}

func (h _htmlInputElement) Accept() string {
	return h.Get("accept").String()
}

func (h _htmlInputElement) SetAccept(accept string) string {
	h.Set("accept", accept)
	return h.Accept()
}

func (h _htmlInputElement) Files() []File {
	if h.Type() != "file" {
		return nil
	}

	v := h.Get("files")
	if v == _null || v == _undefined {
		return nil
	}

	size := v.Length()

	if v.Length() <= 0 {
		return nil
	}

	ret := make([]File, size)
	for i := 0; i < size; i++ {
		ret[i] = FileOf(v.Index(i))
	}

	return ret
}

// Methods

// Events

// HTMLInputElementOf convert js.Value to HTMLInputElement
func HTMLInputElementOf(v JSValue) HTMLInputElement {
	return _htmlInputElement{
		HTMLElementOf(v),
	}
}
