// +build js,wasm

package dairaga

// DOMRect javascript DOMRect
type DOMRect struct {
	x      int
	y      int
	width  int
	height int
	top    int
	right  int
	bottom int
	left   int
}

// X the x coordinate of the DOMRect's origin.
func (r DOMRect) X() int {
	return r.x
}

// Y the y coordinate of the DOMRect's origin.
func (r DOMRect) Y() int {
	return r.y
}

// Width the width of the DOMRect.
func (r DOMRect) Width() int {
	return r.width
}

// Height the height of the DOMRect.
func (r DOMRect) Height() int {
	return r.height
}

// Top returns the top coordinate value of the DOMRect (has the same value as y, or y + height if height is negative.)
func (r DOMRect) Top() int {
	return r.top
}

// Right returns the right coordinate value of the DOMRect (has the same value as x + width, or x if width is negative.)
func (r DOMRect) Right() int {
	return r.right
}

// Bottom returns the bottom coordinate value of the DOMRect (has the same value as y + height, or y if height is negative.)
func (r DOMRect) Bottom() int {
	return r.bottom
}

// Left returns the left coordinate value of the DOMRect (has the same value as x, or x + width if width is negative.)
func (r DOMRect) Left() int {
	return r.left
}

// DOMRectOf convert js.Value to DOMRect
func DOMRectOf(v JSValue) DOMRect {
	return DOMRect{
		x:      v.Get("x").Int(),
		y:      v.Get("y").Int(),
		width:  v.Get("width").Int(),
		height: v.Get("height").Int(),
		top:    v.Get("top").Int(),
		right:  v.Get("right").Int(),
		bottom: v.Get("bottom").Int(),
		left:   v.Get("left").Int(),
	}
}
