// +build js,wasm

package dairaga

import (
	"fmt"
	"strings"
	"syscall/js"
	"time"
)

var _global = ValueOf(js.Global())

// Global return js.Global
func Global() JSValue {
	return _global
}

var _undefined = ValueOf(js.Undefined())

// Undefined return javascript undefined
func Undefined() JSValue {
	return _undefined
}

var _null = ValueOf(js.Null())

// Null return javascript null
func Null() JSValue {
	return _null
}

// New call javascript new to create an object
func New(class string, a ...interface{}) JSValue {
	return _global.Get(class).New(a...)
}

// Alert javascript alert
func Alert(a ...string) {
	str := strings.Join(a, "\n")
	_global.Call("alert", str)
}

func init() {
	fmt.Printf("%s - %v\n", "v0.0.1", time.Now())
}
