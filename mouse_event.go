// +build js,wasm

package dairaga

// Mouse Button (https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button)
const (
	ButtonMain      = iota // 0
	ButtonAuxiliary        // 1
	ButtonSecondary        // 2
	ButtonFourth           // 3
	ButtonFifth            // 4
)

// Mosue Buttons (https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons)
const (
	ButtonsNoButton = 1 << iota
	ButtonsPrimary
	ButtonsSecondary
	ButtonsAuxilary
	ButtonsFourth
	ButtonsFifth
)

// MouseEvent javascript MouseEvent
type MouseEvent interface {
	UIEvent

	// properties:
	AltKey() bool
	Button() int
	Buttons() int
	ClientX() float64
	ClientY() float64
	CtrlKey() bool
	MetaKey() bool
	MovementX() float64
	MovementY() float64
	OffsetX() float64
	OffsetY() float64
	PageX() float64
	PageY() float64
	Region() string
	RelatedTarget() EventTarget
	ScreenX() float64
	ScreenY() float64
	ShiftKey() bool
	X() float64
	Y() float64
	// methods:
	GetModifierState(string) bool
}

type _mouseEvent struct {
	UIEvent
}

// MouseEventOf convert js.Value to MouseEvent
func MouseEventOf(v JSValue) MouseEvent {
	return _mouseEvent{UIEventOf(v)}
}

// Properties

func (e _mouseEvent) AltKey() bool {
	return e.Get("altKey").Bool()
}

func (e _mouseEvent) Button() int {
	return e.Get("button").Int()
}

func (e _mouseEvent) Buttons() int {
	return e.Get("buttons").Int()
}

func (e _mouseEvent) ClientX() float64 {
	return e.Get("clientX").Float()
}

func (e _mouseEvent) ClientY() float64 {
	return e.Get("clientY").Float()
}

func (e _mouseEvent) CtrlKey() bool {
	return e.Get("ctrlKey").Bool()
}

func (e _mouseEvent) MetaKey() bool {
	return e.Get("metaKey").Bool()
}

func (e _mouseEvent) MovementX() float64 {
	return e.Get("movementX").Float()
}

func (e _mouseEvent) MovementY() float64 {
	return e.Get("movementY").Float()
}

func (e _mouseEvent) OffsetX() float64 {
	return e.Get("offsetX").Float()
}

func (e _mouseEvent) OffsetY() float64 {
	return e.Get("offsetY").Float()
}

func (e _mouseEvent) PageX() float64 {
	return e.Get("pageX").Float()
}

func (e _mouseEvent) PageY() float64 {
	return e.Get("pageY").Float()
}

func (e _mouseEvent) Region() string {
	return e.Get("region").String()
}

func (e _mouseEvent) RelatedTarget() EventTarget {
	return EventTargetOf(e.Get("relatedTarget"))
}

func (e _mouseEvent) ScreenX() float64 {
	return e.Get("screenX").Float()
}

func (e _mouseEvent) ScreenY() float64 {
	return e.Get("screenY").Float()
}

func (e _mouseEvent) ShiftKey() bool {
	return e.Get("shiftKey").Bool()
}

func (e _mouseEvent) X() float64 {
	return e.ClientX()
}

func (e _mouseEvent) Y() float64 {
	return e.ClientY()
}

// Methods:

func (e _mouseEvent) GetModifierState(keyArg string) bool {
	return e.Call("getModifierState", keyArg).Bool()
}
