// +build js,wasm

package dairaga

import (
	"syscall/js"
	"testing"
)

func TestBool(t *testing.T) {
	v := js.ValueOf(true)

	v2 := ValueOf(v)

	if v2.Bool() != true {
		t.Error("Bool() faliure")
	}
}
