// +build js,wasm

package dairaga

// UIEvent javascript UIEvent (https://developer.mozilla.org/en-US/docs/Web/API/UIEvent)
type UIEvent interface {
	Event

	// properties:
	Detail() int
}

type _uiEvent struct {
	Event
}

// Properties

func (e _uiEvent) Detail() int {
	return e.Get("detail").Int()
}

// UIEventOf convert js.Value to UIEvent
func UIEventOf(v JSValue) UIEvent {
	return _uiEvent{EventOf(v)}
}
