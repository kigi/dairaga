// +build js,wasm

package dairaga

import (
	"syscall/js"
)

func makeArgs2(x, y interface{}, a ...interface{}) []interface{} {
	size := len(a)
	if size <= 0 {
		return []interface{}{x, y}
	}

	ret := make([]interface{}, size+2)
	ret[0] = x
	ret[1] = y
	copy(ret[2:], a)
	return ret
}

func stringIterToSlice(v JSValue) []string {
	var ret []string

	for x := v.Call("next"); !x.Get("done").Bool(); x = v.Call("next") {
		ret = append(ret, x.Get("value").String())
	}

	return ret
}

func nodeIterToSlice(v JSValue) []Node {
	var ret []Node

	for x := v.Call("next"); !x.Get("done").Bool(); x = v.Call("next") {
		ret = append(ret, NodeOf(v.Get("value")))
	}
	return ret
}

func elementIterToSlice(v JSValue) []Element {
	var ret []Element

	for x := v.Call("next"); !x.Get("done").Bool(); x = v.Call("next") {
		ret = append(ret, ElementOf(v.Get("value")))
	}
	return ret
}

func stringsToInterfaceValues(x1 string, x []string) []interface{} {
	size := len(x) + 1
	ret := make([]interface{}, size)
	ret[0] = x1
	for i := 1; i < size; i++ {
		ret[i] = x[i-1]
	}

	return ret
}

func callInterfaceValue(obj JSValue, m string, i interface{}) JSValue {
	switch v := i.(type) {
	case string:
		return obj.Call(m, v)
	case JSValue:
		return obj.Call(m, v.ToValue())
	default:
		return obj.Call(m, js.ValueOf(i))
	}
}

func collectionToElements(c JSValue) []Element {
	size := c.Length()
	if size <= 0 {
		return nil
	}

	ret := make([]Element, size)
	for i := 0; i < size; i++ {
		ret[i] = ElementOf(c.Index(i))
	}
	return ret
}

func addEventCB(obj EventTarget, evt string, f func(Event)) js.Callback {
	cb := js.NewCallback(func(args []js.Value) {
		evt := EventOf(ValueOf(args[0]))
		f(evt)
	})
	obj.AddEventListener(evt, cb)
	return cb
}

func addMouseEventCB(obj EventTarget, evt string, f func(MouseEvent)) js.Callback {
	cb := js.NewCallback(func(args []js.Value) {
		evt := MouseEventOf(ValueOf(args[0]))
		f(evt)
	})
	obj.AddEventListener(evt, cb)
	return cb
}
