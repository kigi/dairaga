// +build js,wasm

package dairaga

// Document javascript Document (https://developer.mozilla.org/en-US/docs/Web/API/Document)
type Document interface {
	Node
	// TODO: properties
	/*
		alinkColor
		anchors
		applets
		async
		bgColor
		characterSet
		childElementCount
		children
		compatMode
		contentType
		currentScript
		defaultView
		designMode
		dir
		doctype
		documentElement
		documentURI
		documentURIObject
		domain
		domConfig
		embeds
		fgColor
		firstElementChild
		forms
		fullscreen
		fullscreenEnabled
		head
		height
		hidden
		images
		implementation
		lastElementChild
		lastModified
		lastStyleSheetSet
		linkColor
		links
		location
		mozSyntheticDocument
		onabort
		onafterscriptexecute
		onanimationcancel
		onanimationend
		onanimationiteration
		onauxclick
		onbeforescriptexecute
		onblur
		onchange
		onclick
		onclose
		oncontextmenu
		ondblclick
		onerror
		onfocus
		onfullscreenchange
		onfullscreenerror
		ongotpointercapture
		oninput
		onkeydown
		onkeypress
		onkeyup
		onload
		onloadend
		onloadstart
		onlostpointercapture
		onmousedown
		onmousemove
		onmouseout
		onmouseover
		onmouseup
		onoffline
		ononline
		onpointercancel
		onpointerdown
		onpointerenter
		onpointerleave
		onpointermove
		onpointerout
		onpointerover
		onpointerup
		onreset
		onresize
		onscroll
		onselect
		onselectionchange
		onselectionchange
		onselectstart
		onsubmit
		ontouchcancel
		ontouchstart
		ontransitioncancel
		ontransitionend
		onvisibilitychange
		onwheel
		origin
		plugins
		policy
		popupNode
		preferredStyleSheetSet
		readyState
		referrer
		scripts
		scrollingElement
		selectedStyleSheetSet
		styleSheetSets
		timeline
		title
		tooltipNode
		URL
		visibilityState
		vlinkColor
		width
		xmlEncoding
		xmlVersion
	*/
	Body() HTMLElement

	// TODO: methods
	/*
		adoptNode()

		createCDATASection()
		createComment()
		createDocumentFragment()
		createElementNS()
		createEntityReference()
		createExpression()
		createNodeIterator()
		createNSResolver()
		createProcessingInstruction()
		createRange()
		createTextNode()
		createTouch()
		createTouchList()
		createTreeWalker()
		enableStyleSheetsForSet()
		evaluate()
		execCommand()
		exitFullscreen()
		exitPointerLock()
		getAnimations()
		getBoxObjectFor()


		hasFocus()
		hasStorageAccess()
		importNode()
		mozSetImageElement()

		queryCommandEnabled()
		queryCommandSupported()
		registerElement()
		releaseCapture()
		requestStorageAccess()
	*/

	Append(interface{})
	Close()
	CreateAttribute(string) Node
	CreateElement(string) Element
	GetElementByID(string) Element
	GetElementsByClassName(string) []Element
	GetElementsByTagName(string) []Element
	GetElementsByTagNameNS(string, string) []Element
	Open()
	Prepend(interface{})
	QuerySelector(string) Element
	QuerySelectorAll(string) ElementList
	Write(string)
	Writeln(string)
}

type _document struct {
	Node
	body HTMLElement
}

// DocumentOf get javascript document object
func DocumentOf(v JSValue) Document {
	return _document{
		NodeOf(v),
		HTMLElementOf(v.Get("body")),
	}
}

// Properties:

func (d _document) Body() HTMLElement {
	return d.body
}

// Methods:

func (d _document) Append(n interface{}) {
	callInterfaceValue(d, "append", n)
}

func (d _document) Close() {
	d.Call("close")
}

func (d _document) CreateAttribute(name string) Node {
	return NodeOf(d.Call("createAttribute", name))
}

func (d _document) CreateElement(tag string) Element {
	e := d.Call("createElement", tag)
	return ElementOf(e)
}

func (d _document) GetElementByID(id string) Element {
	e := d.Call("getElementById", id)
	return ElementOf(e)
}

func (d _document) collect(m string, args ...interface{}) []Element {
	v := d.Call(m, args...)
	if v == _null || v == _undefined {
		return nil
	}

	return collectionToElements(v)
}

func (d _document) GetElementsByClassName(name string) []Element {
	return d.collect("getElementsByClassName", name)
}

func (d _document) GetElementsByName(name string) []Element {
	return d.collect("getElementsByName", name)
}

func (d _document) GetElementsByTagName(name string) []Element {
	return d.collect("getElementsByTagName", name)
}

func (d _document) GetElementsByTagNameNS(ns, name string) []Element {
	return d.collect("getElementsByTagNameNS", ns, name)
}

func (d _document) Open() {
	d.Call("open")
}

func (d _document) Prepend(n interface{}) {
	callInterfaceValue(d, "prepend", n)
}

func (d _document) QuerySelector(q string) Element {
	return ElementOf(d.Call("querySelector", q))
}

func (d _document) QuerySelectorAll(q string) ElementList {
	return ElementListOf(d.Call("querySelectorAll", q))
}

func (d _document) Write(line string) {
	d.Call("write", line)
}

func (d _document) Writeln(line string) {
	d.Call("writeln", line)
}
