// +build js,wasm

package dairaga

import (
	"syscall/js"
)

// DOMTokenList javascript DOMTokenList (only for string token)
type DOMTokenList interface {
	JSValue

	// properties:
	Value() string

	// TODO: methods:
	Add(string, ...string)
	Contains(string) bool
	Entries() []string
	ForEach(func(string, int, interface{}, interface{}), interface{})
	Item(int) string
	Remove(string, ...string)
	Supports(string) bool
	Toggle(string, bool) bool
	Values() []string
}

type _domTokenList struct {
	JSValue
}

// DOMTokenListOf convert js.Value to DOMTokenList
func DOMTokenListOf(v JSValue) DOMTokenList {
	return _domTokenList{v}
}

// Properties:

func (d _domTokenList) Value() string {
	return d.Get("value").String()
}

// Methods:

func (d _domTokenList) Add(t1 string, tokens ...string) {
	size := len(tokens) + 1
	if size == 1 {
		d.Call("add", t1)
		return
	}

	d.Call("add", stringsToInterfaceValues(t1, tokens)...)
}

func (d _domTokenList) Contains(token string) bool {
	return d.Call("contains", token).Bool()
}

func (d _domTokenList) Entries() []string {
	return stringIterToSlice(d.Call("entries"))

}

func (d _domTokenList) ForEach(h func(string, int, interface{}, interface{}), arg interface{}) {
	cb := js.NewCallback(func(args []js.Value) {
		h(args[0].String(), args[1].Int(), args[2], arg)
	})
	d.Call("forEach", cb, arg)
	cb.Release()
}

func (d _domTokenList) Item(i int) string {
	return d.Call("item", i).String()
}

func (d _domTokenList) Keys() []string {
	return stringIterToSlice(d.Call("keys"))
}

func (d _domTokenList) Remove(t1 string, tokens ...string) {
	d.Call("remove", stringsToInterfaceValues(t1, tokens)...)
}

func (d _domTokenList) Supports(token string) bool {
	return d.Call("supports", token).Bool()
}

func (d _domTokenList) Toggle(token string, force bool) bool {
	return d.Call("toggle", token, force).Bool()
}

func (d _domTokenList) Values() []string {
	return stringIterToSlice(d.Call("values"))
}
