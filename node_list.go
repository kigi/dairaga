// +build js,wasm

package dairaga

import "syscall/js"

// NodeList javascript NodeList
type NodeList interface {
	JSValue

	//Entries() []Node
	ForEach(func(Node, int, interface{}, interface{}), interface{})
	Item(int) Node
	// Keys() []int
	Values() []Node
}

type _nodeList struct {
	JSValue
}

// NodeListOf convert js.Value to NodeList
func NodeListOf(v JSValue) NodeList {
	return _nodeList{v}
}

func (n _nodeList) ForEach(h func(Node, int, interface{}, interface{}), arg interface{}) {
	cb := js.NewCallback(func(args []js.Value) {
		h(NodeOf(ValueOf(args[0])), args[1].Int(), args[2], arg)
	})
	n.Call("forEach", cb, arg)
	cb.Release()
}

func (n _nodeList) Item(i int) Node {
	return NodeOf(n.Call("item", i))
}

func (n _nodeList) Values() []Node {
	return nodeIterToSlice(n.Call("values"))
}
