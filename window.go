package dairaga

import (
	"syscall/js"
)

// Window javascript Window (https://developer.mozilla.org/en-US/docs/Web/API/Window)
type Window interface {

	// TODO: properties
	/*
		applicationCache
		caches
		closed
		console
		controllers
		crypto
		customElements
		defaultStatus
		devicePixelRatio
		dialogArguments
		directories
		document
		frameElement
		frames
		fullScreen
		history
		indexedDB
		innerHeight
		innerWidth
		isSecureContext
		isSecureContext
		length
		localStorage
		location
		locationbar
		menubar
		mozAnimationStartTime
		mozInnerScreenX
		mozInnerScreenY
		mozPaintCount
		name
		navigator
		onabort
		onafterprint
		onanimationcancel
		onanimationend
		onanimationiteration
		onappinstalled
		onauxclick
		onbeforeinstallprompt
		onbeforeprint
		onbeforeunload
		onblur
		onchange
		onclick
		onclose
		oncontextmenu
		ondblclick
		ondevicelight
		ondevicemotion
		ondeviceorientation
		ondeviceorientationabsolute
		ondeviceproximity
		ondragdrop
		onerror
		onfocus
		ongamepadconnected
		ongamepaddisconnected
		ongotpointercapture
		onhashchange
		oninput
		onkeydown
		onkeypress
		onkeyup
		onlanguagechange
		onload
		onloadend
		onloadstart
		onlostpointercapture
		onmessage
		onmessageerror
		onmousedown
		onmousemove
		onmouseout
		onmouseover
		onmouseup
		onmozbeforepaint
		onpaint
		onpointercancel
		onpointerdown
		onpointerenter
		onpointerleave
		onpointermove
		onpointerout
		onpointerover
		onpointerup
		onpopstate
		onrejectionhandled
		onreset
		onresize
		onscroll
		onselect
		onselectionchange
		onselectstart
		onstorage
		onsubmit
		ontouchcancel
		ontouchstart
		ontransitioncancel
		ontransitionend
		onunhandledrejection
		onunload
		onuserproximity
		onvrdisplayactivate
		onvrdisplayblur
		onvrdisplayconnect
		onvrdisplaydeactivate
		onvrdisplaydisconnect
		onvrdisplayfocus
		onvrdisplaypresentchange
		onwheel
		opener
		origin
		outerHeight
		outerWidth
		pageYOffset
		parent
		performance
		personalbar
		pkcs11
		screen
		screenX
		screenY
		scrollbars
		scrollMaxX
		scrollMaxY
		scrollX
		scrollY
		self
		sessionStorage
		sidebar
		speechSynthesis
		status
		statusbar
		toolbar
		top
		visualViewport
		window
	*/

	// TODO: methods
	/*
		atob()
		back()
		blur()
		btoa()
		cancelAnimationFrame()
		cancelIdleCallback()
		captureEvents()
		clearImmediate()
		clearInterval()
		clearTimeout()
		close()
		confirm()
		convertPointFromNodeToPage()
		convertPointFromPageToNode
		createImageBitmap()
		dump()
		event
		fetch()
		find()
		focus()
		forward()
		getAttention()
		getComputedStyle()
		getDefaultComputedStyle()
		getSelection()
		home()
		matchMedia()
		minimize()
		moveBy()
		moveTo()
		open()
		openDialog()
		postMessage()
		print()
		prompt()
		releaseEvents()
		requestAnimationFrame()
		requestFileSystem()
		requestIdleCallback()
		resizeBy()
		resizeTo()
		restore()
		routeEvent()
		scroll()
		scrollBy()
		scrollByLines()
		scrollByPages()
		scrollTo()
		setCursor()
		setImmediate()
		setInterval()
		setTimeout()
		showModalDialog()
		sizeToContent()
		stop()
		updateCommands()
	*/
	Alert(a ...string)
}

type _window js.Value

func (w _window) Alert(a ...string) {
	Alert(a...)
}

var window = _window(js.Global())

// WindowOf get javascript window object
func WindowOf() Window {
	return window
}
