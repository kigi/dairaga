// +build js,wasm

package dairaga

import "syscall/js"

// ElementList javascript Element of NodeList
type ElementList interface {
	JSValue

	//Entries() []Node
	ForEach(func(Element, int, interface{}, interface{}), interface{})
	Item(int) Element
	// Keys() []int
	Values() []Element
}

type _elementList struct {
	JSValue
}

// ElementListOf convert js.Value to Emenet of NodeList
func ElementListOf(v JSValue) ElementList {
	return _elementList{v}
}

func (n _elementList) ForEach(h func(Element, int, interface{}, interface{}), arg interface{}) {
	cb := js.NewCallback(func(args []js.Value) {
		h(ElementOf(ValueOf(args[0])), args[1].Int(), args[2], arg)
	})
	n.Call("forEach", cb, arg)
	cb.Release()
}

func (n _elementList) Item(i int) Element {
	return ElementOf(n.Call("item", i))
}

func (n _elementList) Values() []Element {
	return elementIterToSlice(n.Call("values"))
}
