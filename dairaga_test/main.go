// +build js,wasm

package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"syscall/js"
	"time"

	"github.com/btcsuite/btcd/btcec"
	"gitlab.com/kigi/dairaga"
)

var keyBytes []uint8

var document = dairaga.DocumentOf(dairaga.ValueOf(js.Global().Get("document")))

func pad(buf []byte, size int) []byte {
	newbuf := make([]byte, 0, size)
	padLength := size - len(buf)
	for i := 0; i < padLength; i++ {
		newbuf = append(newbuf, 0)
	}
	return append(newbuf, buf...)
}

func serializeCompact(sig *btcec.Signature) []byte {
	b := make([]byte, 0, 64)
	// TODO: Padding
	rbytes := pad(sig.R.Bytes(), 32)
	sbytes := pad(sig.S.Bytes(), 32)
	b = append(b, rbytes...)
	b = append(b, sbytes...)
	if len(b) != 64 {
		panic("Invalid signature length")
	}
	return b
}

func main() {

	div1 := dairaga.HTMLElementOf(document.CreateElement("div"))
	html1 := dairaga.HTML(`"<br />{{.}} [a <:>&b]"`, time.Now())
	div1.SetInnerHTML(html1)

	div2 := document.CreateElement("div")
	html2 := dairaga.HTML(`"<br />{{.}}"<br /><input type="file" name="Def" id="def" value="abc">`, rand.Int())
	div2.SetInnerHTML(html2)

	document.Body().AppendChild(div1).AppendChild(div2)

	fmt.Println("baseURI:", div2.BaseURI())

	privFile := dairaga.HTMLElementOf(document.GetElementByID("priv"))
	privFile.OnChange(func(evt dairaga.Event) {
		fmt.Println("evt type", evt.Type())
		fmt.Println("target", evt.Target())

		fileInput := dairaga.HTMLInputElementOf(evt.Target())
		tmpFile := fileInput.Files()[0]
		fmt.Println("last: ", tmpFile.LastModified(), "type: ", tmpFile.Type())

		reader := dairaga.NewFileReader()
		reader.OnBufferLoaded(func(buf []byte) {
			fmt.Println(buf)

			curve := btcec.S256()

			priv, _ := btcec.PrivKeyFromBytes(curve, buf)

			msg := "hello world!"
			hash := sha256.Sum256([]byte(msg))
			signature, err := priv.Sign(hash[:])
			if err != nil {
				log.Println(err)
				return
			}

			log.Println("signature:", hex.EncodeToString(serializeCompact(signature)))

		})

		reader.ReadAsArrayBuffer(tmpFile)
		fmt.Println(keyBytes)
	})

	fmt.Println(dairaga.IsArray(dairaga.Global()))
	fmt.Println(js.Global().Get("byteLength"))
	fmt.Println(js.Global().Get("byteLength") == js.Undefined())
	input1 := dairaga.HTMLInputElementOf(document.GetElementByID("def"))
	fmt.Println(input1.Name(), input1.Type(), input1.Value(), input1.Disabled(), input1.Checked(), input1.Accept(), input1.Files())

	select {}
}
