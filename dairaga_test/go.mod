module dairaga_test

require (
	github.com/btcsuite/btcd v0.0.0-20181013004428-67e573d211ac
	gitlab.com/kigi/dairaga v0.1.0
)

replace gitlab.com/kigi/dairaga => ../
