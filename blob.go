// +build js,wasm

package dairaga

// Blob javascript Blob (https://developer.mozilla.org/en-US/docs/Web/API/Blob)
type Blob interface {
	JSValue
	// properties
	Size() int
	Type() string

	// methods:
	Slice(int, int, string) Blob
}

type _blob struct {
	JSValue
}

// BlobOf convert js.Value to Blob
func BlobOf(v JSValue) Blob {
	return _blob{v}
}

func (b _blob) Size() int {
	return b.Get("size").Int()
}

func (b _blob) Type() string {
	return b.Get("type").String()
}

func (b _blob) Slice(start, end int, contentType string) Blob {
	return BlobOf(b.Call("slice", start, end, contentType))
}
