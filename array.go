// +build js,wasm

package dairaga

import (
	"fmt"
	"syscall/js"
)

/**
	Array Convert (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
**/

// IsArray verify input is array or not
func IsArray(v JSValue) bool {
	return _global.Get("Array").Call("isArray", v.ToValue()).Bool()
}

// StringSliceOf convert js.Value to StringSlice
func StringSliceOf(v JSValue) []string {
	if !IsArray(v) {
		fmt.Println("input is not an array")
		return nil
	}

	len := v.Length()

	ret := make([]string, len)
	for i := 0; i < len; i++ {
		ret[i] = v.Index(i).String()
	}

	return ret
}

// ByteSliceOf convert javascript array buffer to golang uint8 slice
func ByteSliceOf(v JSValue) []byte {
	size := v.Get("byteLength")
	if size == _undefined {
		fmt.Println("input is not an ArrayBuffer")
		return nil
	}

	ret := make([]uint8, size.Int())
	destArray := js.TypedArrayOf(ret)

	srcArray := New("Uint8Array", v.ToValue())

	destArray.Call("set", srcArray.ToValue(), 0)

	destArray.Release()

	return []byte(ret)
}
