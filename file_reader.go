// +build js,wasm

package dairaga

import "syscall/js"

// FileReader javascript FileReader (https://developer.mozilla.org/en-US/docs/Web/API/FileReader)
type FileReader interface {
	EventTarget
	// TODO: properties
	/*
		error
		onload: seperate OnBufferLoaded, BinaryLoaded, DataURLLoaded, and TextLoaded
		readyState
		result
	*/
	OnBufferLoaded(func([]byte))

	// TODO: methods
	/*
		abort()
		readAsBinaryString()
		readAsDataURL()
		readAsText()
	*/
	ReadAsArrayBuffer(File) JSValue
}

type _fileReader struct {
	EventTarget
}

func (f _fileReader) ReadAsArrayBuffer(file File) JSValue {
	return f.Call("readAsArrayBuffer", file.ToValue())
}

func (f _fileReader) OnBufferLoaded(handler func([]byte)) {
	f.AddEventListener("load", js.NewCallback(func(args []js.Value) {
		evt := EventOf(ValueOf(args[0]))
		handler(ByteSliceOf(evt.Target().Get("result")))
	}))
}

// NewFileReader ...
func NewFileReader() FileReader {

	return _fileReader{
		EventTargetOf(New("FileReader")),
	}
}
