// +build js,wasm

package dairaga

import "syscall/js"

// EventTarget javascript EventTarget (https://developer.mozilla.org/en-US/docs/Web/API/EventTarget)
type EventTarget interface {
	JSValue
	AddEventListener(string, js.Callback, ...interface{})
	RemoveEventListener(string, js.Callback, ...interface{})
	DispatchEvent(Event) bool
}

type _eventTarget struct {
	JSValue
}

// EventTargetOf convert js.Value to EventTarget
func EventTargetOf(v JSValue) EventTarget {
	return _eventTarget{v}
}

func (e _eventTarget) AddEventListener(typ string, cb js.Callback, a ...interface{}) {
	args := makeArgs2(typ, cb, a)
	e.Call("addEventListener", args...)
}

func (e _eventTarget) RemoveEventListener(typ string, cb js.Callback, a ...interface{}) {
	args := makeArgs2(typ, cb, a)
	e.Call("removeEventListener", args...)
}

func (e _eventTarget) DispatchEvent(evt Event) bool {
	return e.Call("dispatchEvent", evt.ToValue()).Bool()
}
